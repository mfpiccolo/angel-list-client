require "./lib/startup"
require 'faraday'
require 'json'

def welcome
  puts "Welcome to the Github command-line client."
  menu
end

def menu
  choice = nil
  until choice == 'e'
    puts "What would you like to do?"
    puts "Press 'v' to view a startups info."
    puts "Press 'e' to exit."

    case choice = gets.chomp
    when 'v'
      view
    when 'e'
      exit
    else
      invalid
    end
  end
end

def view
  puts "Please enter in the id of the startup:"
  id = gets.chomp
  startup = Startup.find(id)
  locations = startup['locations'].first
  puts "#{startup['name']} is located in #{locations['display_name']}"
end

welcome