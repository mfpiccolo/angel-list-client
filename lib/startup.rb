class Startup
  def self.find(id)
    url = "https://api.angel.co/1/startups/#{id}"
    post_response = Faraday.get(url)

    startup = JSON.parse(post_response.body)
  end
end