require 'spec_helper'


describe Startup do 
  context '.find' do 
    it 'retrieves a startups information' do
      id = '6706'
      stub = stub_request(:get, "https://api.angel.co/1/startups/6706").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.8.6'}).
         to_return(:status => 200, :body => "", :headers => {})
      Startup.find(id)
      WebMock.should have_requested(:get, "https://api.angel.co/1/startups/6706").with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.8.6'})
    end
  end
end